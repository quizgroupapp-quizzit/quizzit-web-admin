import {applyMiddleware, createStore} from 'redux';
import thunk from "redux-thunk";
import loggerMiddleware from '../shared/middlewares/logger-middleware'
import rootReducer from "../shared/reducers";
import {
    getFirebase,
} from 'react-redux-firebase';

export const store = createStore(
    rootReducer,
    applyMiddleware(thunk.withExtraArgument({getFirebase}), loggerMiddleware)
);
