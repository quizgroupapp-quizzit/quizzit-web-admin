import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import {firebaseKeys} from "../shared/constaints/firebaseKeys";

const firebaseConfig = {
    apiKey: firebaseKeys.API_KEY,
    authDomain: firebaseKeys.AUTH_DOMAIN,
    projectId: firebaseKeys.PROJECT_ID,
    storageBucket: firebaseKeys.STORAGE_BUCKET,
    messagingSenderId: firebaseKeys.MESSAGE_SENDER_ID,
    appId: firebaseKeys.APP_ID,
    measurementId: firebaseKeys.MEASURE_ID
};

firebase.initializeApp(firebaseConfig);

export default firebase;