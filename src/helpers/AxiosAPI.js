import axios from "axios";
import {token} from "../shared/constaints/token";
import firebase from "firebase";
import {firebaseKeys} from "../shared/constaints/firebaseKeys";
import {API as api} from "../shared/constaints/api";

const axiosInstance = axios.create({
    baseURL: api.BASE_URL,
});

const refreshAccessToken = async () => {
    const res = await axios.post(`https://securetoken.googleapis.com/v1/token?key=${firebaseKeys.API_KEY}`, {
        grant_type: 'refresh_token',
        refresh_token: localStorage.getItem(token.REFRESH_TOKEN),
    })
    return res.data
}

axiosInstance.interceptors.request.use(
    async(config) => {
        // config.headers.Authorization = `Bearer ${localStorage.getItem(token.CUSTOM_TOKEN)}`;
        await firebase.auth().currentUser.getIdToken() //true
            .then(idToken => {
                config.headers.Authorization = `Bearer ${idToken}`;
            })
        return config;
    },
    error => {
        return Promise.reject(error);
    }
)

axiosInstance.interceptors.response.use(
    response => {
        return(response)
    },
    async function (error) {
        const originalRequest = error.config;
        if (error?.response?.status === 401) {
            // originalRequest._retry = true;
            // firebase.auth().currentUser.getIdToken() //true
            //     .then(idToken => {
            //         localStorage.setItem(token.CUSTOM_TOKEN, idToken);
            //         originalRequest.headers['Authorization'] = `Bearer ${idToken}`;
            //         return axios.request(originalRequest);
            //     })
        }
        return Promise.reject(error);
    }
);

export default axiosInstance;


