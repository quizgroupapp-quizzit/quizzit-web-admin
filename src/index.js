import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {store} from './app/store';
import {Provider, useSelector} from 'react-redux';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import {loadIcons} from "./helpers/icon-loader";
import 'firebase/auth'
import firebase from "./config/firebaseConfig";
import {
    isLoaded,
    ReactReduxFirebaseProvider,
} from 'react-redux-firebase';


loadIcons();

// react-redux-firebase config
const rrfConfig = {
    userProfile: 'users'
    // useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB
    // enableClaims: true // Get custom claims along with the profile
}

// Initialize firebase instance

const rrfProps = {
    firebase,
    // config: rrfConfig,
    config: {},
    dispatch: store.dispatch,
};

function AuthIsLoaded({ children }) {
    const auth = useSelector(state => state.firebase.auth);
    if (!isLoaded(auth))
        return (
            <div className="text-center">
                <div
                    className="spinner-grow text-primary"
                    style={{ width: "7rem", height: "7rem" }}
                    role="status"
                >
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );
    return children;
}

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <ReactReduxFirebaseProvider {...rrfProps}>
                <AuthIsLoaded>
                <App/>
                </AuthIsLoaded>
            </ReactReduxFirebaseProvider>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
