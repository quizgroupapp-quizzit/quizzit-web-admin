import {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import DashBoard from "../pages/dashboard/DashBoard";
import Login from "../pages/login/Login";
import "../style/mainpage/MainPage.css"
import "react-toastify/dist/ReactToastify.css";
import {ToastContainer} from "react-toastify";
import ErrorScreen from "../shared/layout/error/ErrorScreen";


export class MainPage extends Component {

    render() {

        return (
            <BrowserRouter>
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
                <Switch>
                    <Route exact path="/error">
                        <ErrorScreen/>
                    </Route>
                    <Route exact path="/login">
                        <Login/>
                    </Route>
                    <Route path="/">
                        <DashBoard/>
                    </Route>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default MainPage;
