import axiosInstance from "../helpers/AxiosAPI";

const initialState = {}

export const ACTION_TYPES = {}

//Reducers
export const mainPageReducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
}

//Actions