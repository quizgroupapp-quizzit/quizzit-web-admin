import {Component} from 'react';
import Header from "../../shared/layout/header/Header";
import {Col, Row} from "react-bootstrap";
import "../../style/pages/dashboard/DashBoard.css"
import Navigation from "../../shared/layout/navigation/Navigation";
import {BrowserRouter} from "react-router-dom";
import Switch from "react-bootstrap/Switch";
import {Redirect, Route} from "react-router";
import SummarySession from "./workspace/summary/SummarySession";
import Footer from "../../shared/layout/footer/Footer";
import ManageUser from "./workspace/manageusers/ManageUser";
import {connect} from "react-redux";
import ManageSubject from "./workspace/managesubjects/ManageSubject";
import ManageGroup from "./workspace/managegroups/ManageGroup";
import ManageQuestion from "./workspace/managequestions/ManageQuestion";

export class DashBoard extends Component {

    render() {
        if (!this.props.uid)
            return <Redirect to="/login"/>

        return (
            <div className="main">
                <div className="containWrap">
                    <BrowserRouter>
                        <Switch className="removeNavPadding">
                            <Route path="/">
                                <Row className="header">
                                    <Col xl={12} className="removeNavPadding">
                                        <Header/>
                                    </Col>
                                </Row>
                                <Row className="">
                                    <Col xl={2} className="sideBar">
                                        <Navigation/>
                                    </Col>
                                    <Col xl={10} className="summaryBackground">
                                        <Route exact path="/">
                                            <SummarySession/>
                                        </Route>
                                        <Route exact path="/manageUser">
                                            <ManageUser/>
                                        </Route>
                                        <Route exact path="/manageGroup">
                                            <ManageGroup/>
                                        </Route>
                                        <Route exact path="/manageQuestion">
                                            <ManageQuestion/>
                                        </Route>
                                        <Route exact path="/manageSubject">
                                            <ManageSubject/>
                                        </Route>
                                    </Col>
                                </Row>
                            </Route>
                        </Switch>
                    </BrowserRouter>
                </div>
                <Row className="footer paddingFooter footerText">
                    <Col xl={12}>
                        <Footer/>
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const uid = state.firebase.auth.uid;
    return {
        uid: uid,
    };
};

export default connect(mapStateToProps)(DashBoard);
