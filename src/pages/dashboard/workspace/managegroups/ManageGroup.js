import {Component} from "react";
import "../../../../style/pages/dashboard/workspace/managegroups/ManageGroup.css";
import {
    Button,
    Col,
    FormControl,
    InputGroup,
    Pagination,
    Row,
    Table} from "react-bootstrap";
import {getEntities, searchGroup, setGroupState} from "./manageGroupReducer";
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField} from "@material-ui/core";
import Autocomplete from '@material-ui/lab/Autocomplete';
import {ITEM_PER_PAGE} from "../../../../shared/constaints/pagination";

export class ManageGroup extends Component {
    state = {
        open: false,
        name: '',
        dialogTitle: '',
        dialogContent: '',
        uid: '',
        order: true,
    }

    componentDidMount() {
        this.props.getEntities(this.props.currentPage);
    }

    handleClickOpen = (name, title, content, uid) => {
        this.setState({
            open: true,
            name: name,
            dialogTitle: title,
            dialogContent: content,
            uid: uid,
        })
    };

    handleClose = () => {
        this.setState({
            open: false
        })
    };

    handleConfirm = () => {
        this.props.setGroupState(this.state.uid);
        this.setState({
            open: false
        })
    }

    handleClickPagination = (clickedPage) => {
        this.props.getEntities(clickedPage);
    }

    handleSearch = (event) => {
        this.props.searchGroup(event.target.value);
    }

    mapper(group, index) {
        return (
            <tr key={group.id}>
                <td className="text-center-v">
                    {(this.props.currentPage - 1) * ITEM_PER_PAGE + index + 1}
                </td>
                <td className="text-center-v">
                    {group.name}
                </td>
                <td className="text-center-v">
                    {group.quizSize}
                </td>
                <td className="text-center-v">
                    {group.createAt.substring(0, 10).split("-", 3).reverse().join("/")}
                </td>
                <td className="text-center-v" style={{width: 250}}>
                    <Autocomplete
                        id="disabled-options-demo"
                        options={group.subjects}
                        getOptionLabel={(option) => option.name}
                        style={{ width: 200 }}
                        renderInput={(params) => (
                            <TextField {...params} label="View subjects..." variant="outlined" />
                        )}
                    />
                </td>
                <td className="text-center-v-h" style={{width: 200}}>
                    {
                        group.isActive
                            ? (
                                <div>
                                    <Button variant="outline-danger"
                                            className="btnUserState"
                                            onClick={() =>
                                                this.handleClickOpen(
                                                    group.name,
                                                    "Ban Confirmation",
                                                    "Do you want to ban",
                                                    group.id)}>
                                        Inactive
                                    </Button>
                                </div>
                            )
                            : (
                                <div>
                                    <Button variant="outline-success"
                                            className="btnUserState"
                                            onClick={() =>
                                                this.handleClickOpen(group.name,
                                                    "Activate Confirmation",
                                                    "Do you want to active",
                                                    group.id)}>
                                        Active
                                    </Button>
                                </div>
                            )
                    }
                </td>
            </tr>
        )
    }

    render() {
        const {entities, totalPages, totalElements} = this.props;
        let items = [];
        for (let number = 1; number <= totalPages; number++) {
            items.push(
                <Pagination.Item key={number}
                                 active={number === this.props.currentPage}
                                 onClick={() => this.handleClickPagination(number)}>
                    {number}
                </Pagination.Item>,
            );
        }
        return (
            <div>
                <Row className="manageUserTitle">
                    <Col xl={1}/>
                    <Col xl={5}>
                        <p>
                            Manage Groups:
                        </p>
                    </Col>
                </Row>
                <Row>
                    <div>
                        <InputGroup className="mb-3">
                            <FormControl aria-describedby="basic-addon1"
                                         placeholder="Search name..."
                                         onChange={event => this.handleSearch(event)}
                                         className="searchBar"/>
                            <InputGroup.Append>
                                <Button className="searchAppend" disabled>
                                    <FontAwesomeIcon icon="search"/>
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </div>
                </Row>
                {totalElements === 0
                    ? <div className="notFound">
                        <p>
                            No element found
                        </p>
                    </div>
                    : <div>
                        <Row className="addTableMargin tableDisplay">
                            <Table responsive bordered className="tableHeight">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Quiz Size</th>
                                    <th>Created At</th>
                                    <th>Subjects</th>
                                    <th className="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.order
                                    ? entities.map((group, index) => this.mapper(group, index))
                                    : entities.map(group => group).reverse().map((group, index) => this.mapper(group, index))}
                                </tbody>
                            </Table>
                            <Dialog
                                open={this.state.open}
                                onClose={this.handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                                BackdropProps={{style: {backgroundColor: 'rgba(0, 0, 0, 0.5)'}}}
                                PaperProps={{
                                    style: {
                                        backgroundColor: '#FFFFFF',
                                        boxShadow: 'none',
                                        width: '40%'
                                    },
                                }}
                            >
                                <DialogTitle
                                    id="alert-dialog-title">
                                    {this.state.dialogTitle}
                                </DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        {`${this.state.dialogContent} ${this.state.name}?`}
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleClose}
                                            variant="danger"
                                            style={{
                                                minWidth: "15%"
                                            }}>
                                        No
                                    </Button>
                                    <Button onClick={this.handleConfirm}
                                            autoFocus
                                            variant="primary"
                                            style={{
                                                minWidth: "15%"
                                            }}>
                                        Yes
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </Row>
                        <Row className="addTableMargin">
                            <Pagination>{items}</Pagination>
                        </Row>
                    </div>}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        currentPage: state.manageGroup.currentPage,
        entities: state.manageGroup.entities,
        totalPages: state.manageGroup.totalPages,
        totalElements: state.manageGroup.totalElements,
    }
}

const mapDispatchToProps = {
    getEntities,
    setGroupState,
    searchGroup,
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageGroup);