import axiosInstance from "../../../../helpers/AxiosAPI";
import {ITEM_PER_PAGE} from "../../../../shared/constaints/pagination";
import {API as api} from "../../../../shared/constaints/api";
import {sorting} from "../../../../shared/constaints/sorting";
import {toast} from "react-toastify";

const initialState = {
    currentPage: 1,
    entities: [],
    totalElements: 0,
    totalPages: 0,
}

export const ACTION_TYPES = {
    GET_GROUPS: 'GetGroups',
    CHANGE_STATUS: 'ChangeGroupStatus',
    CHANGE_STATUS_FAILED: 'ChangeGroupStatusFailed',
    SEARCH_GROUP: 'SearchGroup',
}

//Reducers
export const manageGroupReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.SEARCH_GROUP:
        case ACTION_TYPES.GET_GROUPS: {
            return {
                ...state,
                currentPage: action.payload.page,
                entities: action.payload.data.content,
                totalElements: action.payload.data.totalElements,
                totalPages: action.payload.data.totalPages,
            }
        }
        case ACTION_TYPES.CHANGE_STATUS: {
            toast.success("Updated success!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return {
                ...state,
            }
        }
        case ACTION_TYPES.CHANGE_STATUS_FAILED: {
            toast.error("Updated failed!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return {
                ...state,
            }
        }
        default:
            return state;
    }
}

//Actions
export const getEntities = activePage => async (dispatch) => {
    await axiosInstance.get(`${api.GROUP}?Page=${activePage}&PageSize=${ITEM_PER_PAGE}&Sort=${sorting.ASCENDING}`)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.GET_GROUPS,
                payload: {
                    data: res.data,
                    page: activePage,
                }
            })
        })
        .catch(() => {
            window.location = '/error';
        })

}

export const setGroupState = (uid) => async (dispatch, getState) => {
    await axiosInstance.put(`${api.GROUP}/${uid}/status`)
        .then(() => {
            dispatch({
                type: ACTION_TYPES.CHANGE_STATUS,
            })
        })
        .catch(() => {
                dispatch({
                    type: ACTION_TYPES.CHANGE_STATUS_FAILED,
                })
            }
        )
    const activePage = getState().manageUser.currentPage;
    axiosInstance.get(`${api.GROUP}?Page=${activePage}&PageSize=${ITEM_PER_PAGE}&Sort=${sorting.ASCENDING}`)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.GET_GROUPS,
                payload: {
                    data: res.data,
                    page: activePage,
                }
            })
        })
        .catch(() => {
            window.location = '/error';
        })
}

export const searchGroup = (searchKey) => async (dispatch) => {
    let searchString = `${api.GROUP}?Page=1&PageSize=${ITEM_PER_PAGE}&Sort=${sorting.ASCENDING}`;
    if (searchKey !== "") {
        searchString = searchString.concat(`&GroupName=${searchKey}`);
    }
    await axiosInstance.get(searchString)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.SEARCH_GROUP,
                payload: {
                    data: res.data,
                    page: 1,
                }
            })
        })
        .catch((error) => {
            console.log(error)
            // window.location = '/error';
        })
}
