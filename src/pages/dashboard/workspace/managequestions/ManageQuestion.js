import React, {Component} from "react";
import "../../../../style/pages/dashboard/workspace/managequestions/ManageQuestion.css";
import {Button, Col, Form, FormControl, InputGroup, Pagination, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary, Collapse,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle, IconButton,
    TextField,
    Typography,
    withStyles
} from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
    createQuestion,
    deleteQuestion,
    getEntities as getQuestions,
    searchQuestion,
    updateQuestion
} from "./manageQuestionReducer";
import {getEntities as getSubjects} from "../managesubjects/manageSubjectReducer";
import CloseIcon from '@material-ui/icons/Close';
import Autocomplete from "@material-ui/lab/Autocomplete";
import {validateTest} from "../../../../helpers/validator";
import {Alert} from "@material-ui/lab";

export class ManageQuestion extends Component {
    SAVE_TYPE = {
        CREATE_NEW: 0,
        UPDATE: 1
    }
    state = {
        open: false,
        openForDelete: false,
        dialogTitle: '',
        dialogContent: '',
        order: true,
        inputList: [{content: "", isCorrect: false}, {content: "", isCorrect: false}],
        question: '',
        subject: '',
        saveType: this.SAVE_TYPE.CREATE_NEW,
        qId: '',
        errMsg: false,
    }

    componentDidMount() {
        this.props.getSubjects();
        this.props.getQuestions(this.props.currentPage);
    }

    //Change content of the input
    handleInputChange = (e, index) => {
        const {name, value} = e.target;
        const list = [...this.state.inputList];
        list[index][name] = value;
        this.setState({
            inputList: list,
        })
    };

    // Change content of the header
    handleChangeQuestion = (event) => {
        const {value} = event.target;
        this.setState({
            question: value,
        })
    }

    //Open input dialog
    handleUpdate = (question, title) => {
        this.closeErr();
        this.setState({
            open: true,
            dialogTitle: title,
            question: question.content,
            inputList: JSON.parse(JSON.stringify(question.answers)),
            subject: this.props.subjects.find(subject => subject.id === question.inSubject),
            saveType: this.SAVE_TYPE.UPDATE,
            qId: question.id,
        })
    }

    handleRadioChange = (e, index) => {
        const list = [...this.state.inputList];
        for (let i = 0; i < list.length; i++) {
            list[i]["isCorrect"] = i === index;
        }
        this.setState({
            inputList: list,
        })
    };

    handleRemove = index => {
        const list = [...this.state.inputList];
        list.splice(index, 1);
        this.setState({
            inputList: list,
        })
    }

    handleAutoComplete = (event, value) => {
        this.setState({
            subject: value,
        })
    }

    handleClickOpen = (title) => {
        this.closeErr();
        this.setState({
            open: true,
            dialogTitle: title,
            question: "",
            subject: "",
            inputList: [{content: "", isCorrect: false}, {content: "", isCorrect: false}],
            saveType: this.SAVE_TYPE.CREATE_NEW,
        })
    };

    handleClose = () => {
        this.setState({
            open: false,
            openForDelete: false,
        })
    };

    handleClickPagination = (clickedPage) => {
        this.props.getQuestions(clickedPage);
    }

    handleSearch = (event) => {
        this.props.searchQuestion(event.target.value);
    }

    handleAddMore = () => {
        this.setState({
            inputList: [...this.state.inputList, {content: "", isCorrect: false}]
        })
    }

    handleDelete = () => {
        this.props.deleteQuestion(this.state.qId);
        this.setState({
            openForDelete: false,
        })
    }

    handleConfirmDelete = (question) => {
        this.setState({
            openForDelete: true,
            qId: question.id,
        })
    }

    mapper(question) {
        const answerArr = ['A.', 'B.', 'C.', 'D.'];
        const useStyles = withStyles((theme) => ({
            root: {
                width: '100%',
            },
            heading: {
                fontSize: theme.typography.pxToRem(15),
                fontWeight: theme.typography.fontWeightRegular,
            },
        }));


        return (
            <div className="answerSection">
                <Accordion className="accordion">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon/>}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className={useStyles.heading}>
                            Question: {question.content}
                        </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            {question.answers.map((answer, index) => {
                                if (answer.isCorrect) {
                                    return (
                                        <p style={{color: "#a6e22e", fontWeight: "bold"}}>
                                            {answerArr[index]}
                                            &nbsp;
                                            {answer.content}
                                        </p>
                                    )
                                }
                                return (
                                    <p>
                                        {answerArr[index]}
                                        &nbsp;
                                        {answer.content}
                                    </p>
                                )
                            })}
                            <p>
                                Subject Category: {this.props.subjects
                                .find(subject => subject.id === question.inSubject).name}
                            </p>
                            <div className="buttonFlex">
                                <Button variant="primary"
                                        onClick={() => this.handleUpdate(question, "Update Question")}
                                        style={{height: "40px", minWidth: "100px", padding: "5px"}}>
                                    <FontAwesomeIcon icon="pen"/>
                                    &nbsp;
                                    Edit
                                </Button>
                                &nbsp;
                                <Button variant="danger"
                                        onClick={() => this.handleConfirmDelete(question)}
                                        style={{height: "40px", minWidth: "100px", padding: "5px"}}>
                                    <FontAwesomeIcon icon="trash"/>
                                    &nbsp;
                                    Remove
                                </Button>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
            </div>
        )
    }

    showErr = () => {
        this.setState({
            errMsg: true,
        })
    }

    closeErr = () => {
        this.setState({
            errMsg: false,
        })
    }

    validateSubmitData = (submitData) => {
        const regex = /[a-zA-Z]|[0-9]/;
        //Business Rule 18 (Question & answer contains at least a digit or a letter)
        let br18 = true;
        //Business Rule 16 (A question must have only 1 correct answer)
        let br16 = false;

        if (!validateTest(submitData.content, regex)) {
            br18 = false;
        }
        const answers = submitData.answers;
        for (let i = 0; i < answers.length; i++) {
            if (!validateTest(answers[i].content, regex)) {
                br18 = false;
            }
            if (br16 === true && answers[i].isCorrect) {
                br16 = false;
            } else {
                if (answers[i].isCorrect) {
                    br16 = true;
                }
            }
        }

        return br18 && br16;
    }


    handleSubmit = (event) => {
        event.preventDefault();
        const submitData = {
            content: this.state.question,
            inSubject: this.state.subject.id,
            answers: this.state.inputList,
        }

        console.log(submitData);
        if (this.validateSubmitData(submitData)) {
            if (this.state.saveType === this.SAVE_TYPE.CREATE_NEW) {
                this.props.createQuestion(submitData);

            } else if (this.state.saveType === this.SAVE_TYPE.UPDATE) {
                this.props.updateQuestion(submitData, this.state.qId)
            }
            this.setState({
                open: false
            })
        } else {
            this.showErr();
        }
    }

    render() {
        const {entities, totalPages, totalElements, subjects} = this.props;
        let items = [];
        for (let number = 1; number <= totalPages; number++) {
            items.push(
                <Pagination.Item key={number}
                                 active={number === this.props.currentPage}
                                 onClick={() => this.handleClickPagination(number)}>
                    {number}
                </Pagination.Item>,
            );
        }

        return (
            <div>
                <Row className="manageUserTitle">
                    <Col xl={1}/>
                    <Col xl={5}>
                        <p>
                            Manage Questions:
                        </p>
                    </Col>
                </Row>
                <Row>
                    <div>
                        <InputGroup className="mb-3">
                            <FormControl aria-describedby="basic-addon1"
                                         placeholder="Search name..."
                                         onChange={event => this.handleSearch(event)}
                                         className="searchBar"/>
                            <InputGroup.Append>
                                <Button className="searchAppend" disabled>
                                    <FontAwesomeIcon icon="search"/>
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </div>
                </Row>
                <Row>
                    <Col xl={9}/>
                    <Col xl={3}>
                        <Button variant="outline-primary"
                                onClick={() => this.handleClickOpen("Create New Question")}>
                            <FontAwesomeIcon icon="plus"/>
                            &nbsp;
                            Create New
                        </Button>
                    </Col>
                </Row>
                <Row className="addTableMargin">
                    <Dialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                        BackdropProps={{style: {backgroundColor: 'rgba(0, 0, 0, 0.5)'}}}
                        PaperProps={{
                            style: {
                                backgroundColor: '#FFFFFF',
                                boxShadow: 'none',
                                width: '40%'
                            },
                        }}
                    >
                        <DialogTitle
                            id="alert-dialog-title">
                            <Collapse in={this.state.errMsg}>
                                <Alert
                                    severity="error"
                                    action={
                                        <IconButton
                                            aria-label="close"
                                            color="inherit"
                                            size="small"
                                            onClick={() => {
                                                this.closeErr();
                                            }}
                                        >
                                            <CloseIcon fontSize="inherit"/>
                                        </IconButton>
                                    }
                                >
                                    Submit data invalid!
                                </Alert>
                            </Collapse>
                            {this.state.dialogTitle}
                        </DialogTitle>
                        <Form onSubmit={event => this.handleSubmit(event)}>
                            <DialogContent>
                                <DialogContentText id="alert-dialog-description">
                                    <FormControl aria-describedby="basic-addon1"
                                                 className="inputQuestion"
                                                 required
                                                 value={this.state.question}
                                                 onChange={event => this.handleChangeQuestion(event)}
                                                 as="textarea"
                                                 placeholder="Input question..."/>
                                    <p className="dividerDown">Answer Options:</p>
                                    {this.state.inputList.map((input, index) => {
                                        return (
                                            <div className="inputGroup">
                                                <input type="radio" name="isCorrect"
                                                       value={index}
                                                       required
                                                       checked={this.state.inputList[index]["isCorrect"]}
                                                       onChange={event => this.handleRadioChange(event, index)}/>
                                                &nbsp;
                                                <FormControl aria-describedby="basic-addon1"
                                                             name="content"
                                                             value={input.content}
                                                             required
                                                             className="inputAnswer"
                                                             placeholder="Input answer..."
                                                             onChange={event => this.handleInputChange(event, index)}/>
                                                &nbsp;
                                                {this.state.inputList.length > 2
                                                && <Button variant="outline-danger" className="controlButton"
                                                           onClick={() => this.handleRemove(index)}>Remove</Button>}
                                                &nbsp;
                                                {(this.state.inputList.length - 1 === index
                                                    && this.state.inputList.length !== 4)
                                                && <Button variant="outline-success" className="controlButton"
                                                           onClick={this.handleAddMore}>Add</Button>}
                                            </div>
                                        )
                                    })}
                                    <p className="dividerDown marginUp">Subject Options:</p>
                                    <Autocomplete
                                        id="disabled-options-demo"
                                        onChange={this.handleAutoComplete}
                                        options={subjects}
                                        getOptionLabel={(option) => option.name}
                                        style={{width: "300px"}}
                                        value={this.state.subject}
                                        renderInput={(params) => (
                                            <TextField {...params} label="View subjects..."
                                                       value={this.state.subject.name}
                                                       variant="outlined" required/>
                                        )}
                                    />
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={this.handleClose}
                                        variant="danger"
                                        style={{
                                            minWidth: "15%"
                                        }}>
                                    No
                                </Button>
                                <Button autoFocus
                                        type="submit"
                                        variant="primary"
                                        style={{
                                            minWidth: "15%"
                                        }}>
                                    Yes
                                </Button>
                            </DialogActions>
                        </Form>
                    </Dialog>


                    <Dialog
                        open={this.state.openForDelete}
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                        BackdropProps={{style: {backgroundColor: 'rgba(0, 0, 0, 0.5)'}}}
                        PaperProps={{
                            style: {
                                backgroundColor: '#FFFFFF',
                                boxShadow: 'none',
                                width: '40%'
                            },
                        }}
                    >
                        <DialogTitle
                            id="alert-dialog-title">
                            {this.state.dialogTitle}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Do you want to delete this question?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose}
                                    variant="danger"
                                    style={{
                                        minWidth: "15%"
                                    }}>
                                No
                            </Button>
                            <Button onClick={() => this.handleDelete()}
                                    autoFocus
                                    variant="primary"
                                    style={{
                                        minWidth: "15%"
                                    }}>
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>
                </Row>
                {totalElements === 0
                    ? <div className="notFound">
                        <p>
                            No element found
                        </p>
                    </div>
                    :
                    <div>
                        <div className="answerWholeSection">
                            {entities.map(entity => this.mapper(entity))}
                        </div>
                        <Row className="addTableMargin">
                            <Pagination>{items}</Pagination>
                        </Row>
                    </div>
                }

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        currentPage: state.manageQuestion.currentPage,
        entities: state.manageQuestion.entities,
        totalPages: state.manageQuestion.totalPages,
        totalElements: state.manageQuestion.totalElements,
        subjects: state.manageSubject.entities,
    }
}

const mapDispatchToProps = {
    getQuestions,
    getSubjects,
    createQuestion,
    updateQuestion,
    deleteQuestion,
    searchQuestion,
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageQuestion);