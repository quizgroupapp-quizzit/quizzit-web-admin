import axiosInstance from "../../../../helpers/AxiosAPI";
import {ITEM_PER_PAGE} from "../../../../shared/constaints/pagination";
import {API as api} from "../../../../shared/constaints/api";
import {sorting} from "../../../../shared/constaints/sorting";
import {toast} from "react-toastify";

const initialState = {
    currentPage: 1,
    entities: [],
    totalElements: 0,
    totalPages: 0,
}

export const ACTION_TYPES = {
    GET_QUESTIONS: 'GetQuestion',
    DELETE_QUESTION: 'DeleteQuestion',
    CHANGE_STATUS_FAILED: 'ChangeQuestionStatusFailed',
    SEARCH_QUESTION: 'SearchQuestion',
    CREATE_QUESTION: 'CreateQuestion',
}

//Reducers
export const manageQuestionReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.SEARCH_QUESTION:
        case ACTION_TYPES.GET_QUESTIONS: {
            return {
                ...state,
                currentPage: action.payload.page,
                entities: action.payload.data.content,
                totalElements: action.payload.data.totalElements,
                totalPages: action.payload.data.totalPages,
            }
        }
        case ACTION_TYPES.CREATE_QUESTION:
        case ACTION_TYPES.CHANGE_STATUS: {
            toast.success("Updated success!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return {
                ...state,
            }
        }
        case ACTION_TYPES.CHANGE_STATUS_FAILED: {
            toast.error("Updated failed!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return {
                ...state,
            }
        }
        default:
            return state;
    }
}

//Actions
export const getEntities = activePage => async (dispatch) => {
    await axiosInstance.get(`${api.ADMIN_QUESTION}?Page=${activePage}&PageSize=${ITEM_PER_PAGE}&Sort=${sorting.ASCENDING}`)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.GET_QUESTIONS,
                payload: {
                    data: res.data,
                    page: activePage,
                }
            })
        })
        .catch(() => {
            window.location = '/error';
        })
}

export const createQuestion = question => async (dispatch, getState) => {
    await axiosInstance.post(`${api.GENERAL_QUESTION}`, [{
        content: question.content,
        inSubject: question.inSubject,
        answers: question.answers,
    }])
        .then(res => {
            dispatch({
                type: ACTION_TYPES.CREATE_QUESTION,
                payload: {
                    data: res.data,
                }
            })
        })
        .catch(() => {
            dispatch({
                type: ACTION_TYPES.CHANGE_STATUS_FAILED
            })
        })

    const activePage = getState().manageQuestion.currentPage;
    const res2 = await axiosInstance.get(`${api.ADMIN_QUESTION}?Page=${activePage}&PageSize=${ITEM_PER_PAGE}&Sort=${sorting.ASCENDING}`)


    dispatch({
        type: ACTION_TYPES.GET_QUESTIONS,
        payload: {
            data: res2.data,
            page: activePage,
        }
    })
}

export const updateQuestion = (question, qId) => async (dispatch, getState) => {
    await axiosInstance.put(`${api.GENERAL_QUESTION}/${qId}`, {
        content: question.content,
        inSubject: question.inSubject,
        answers: question.answers,
        isPrivate: false,
    })
        .then(res => {
            dispatch({
                type: ACTION_TYPES.CREATE_QUESTION,
                payload: {
                    data: res.data,
                }
            })
        })
        .catch(err => {
            dispatch({
                type: ACTION_TYPES.CHANGE_STATUS_FAILED
            })
        })

    const activePage = getState().manageQuestion.currentPage;
    const res2 = await axiosInstance.get(`${api.ADMIN_QUESTION}?Page=${activePage}&PageSize=${ITEM_PER_PAGE}&Sort=${sorting.ASCENDING}`)


    dispatch({
        type: ACTION_TYPES.GET_QUESTIONS,
        payload: {
            data: res2.data,
            page: activePage,
        }
    })
}

export const deleteQuestion = (qId) => async (dispatch, getState) => {
    await axiosInstance.delete(`${api.GENERAL_QUESTION}/${qId}`)
        .then(() => {
            dispatch({
                type: ACTION_TYPES.DELETE_QUESTION,
            })
        })
        .catch(() => {
                dispatch({
                    type: ACTION_TYPES.CHANGE_STATUS_FAILED,
                })
            }
        )
    const activePage = getState().manageQuestion.currentPage;
    const res2 = await axiosInstance.get(`${api.ADMIN_QUESTION}?Page=${activePage}&PageSize=${ITEM_PER_PAGE}&Sort=${sorting.ASCENDING}`)


    dispatch({
        type: ACTION_TYPES.GET_QUESTIONS,
        payload: {
            data: res2.data,
            page: activePage,
        }
    })
}

export const searchQuestion = (searchKey) => async (dispatch) => {
    let searchString = `${api.ADMIN_QUESTION}?Page=1&PageSize=${ITEM_PER_PAGE}&Sort=${sorting.ASCENDING}`;
    if (searchKey !== "") {
        searchString = searchString.concat(`&content=${searchKey}`);
    }
    const res = await axiosInstance
        .get(searchString);

    dispatch({
        type: ACTION_TYPES.SEARCH_QUESTION,
        payload: {
            data: res.data,
            page: 1,
        }
    })

}
