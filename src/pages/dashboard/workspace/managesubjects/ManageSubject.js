import {Component} from "react";
import {Col, Row, Table} from "react-bootstrap";
import {getEntities} from "./manageSubjectReducer";
import {connect} from "react-redux";

export class ManageSubject extends Component {
    componentDidMount() {
        this.props.getEntities();
    }

    mapper(subject, index) {
        return (
            <tr key={subject.id}>
                <td>
                    {index + 1}
                </td>
                <td>
                    {subject.name}
                </td>
            </tr>
        )
    }

    render() {
        const {entities} = this.props;

        return (
            <div>
                <Row className="manageUserTitle">
                    <Col xl={1}/>
                    <Col xl={5}>
                        <p>
                            Manage Subjects:
                        </p>
                    </Col>
                </Row>
                <Row className="addTableMargin">
                    <Table responsive bordered>
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        {entities.map((subject, index) => this.mapper(subject, index))}
                        </tbody>
                    </Table>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        entities: state.manageSubject.entities,
    }
}

const mapDispatchToProps = {
    getEntities,
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageSubject);