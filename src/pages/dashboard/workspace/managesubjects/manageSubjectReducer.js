import axiosInstance from "../../../../helpers/AxiosAPI";
import {API as api} from "../../../../shared/constaints/api";

const initialState = {
    entities: [],
}

export const ACTION_TYPES = {
    GET_SUBJECTS: 'GetSubjects',
}

//Reducers
export const manageSubjectReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.GET_SUBJECTS: {
            return {
                ...state,
                entities: action.payload,
            }
        }
        default:
            return state;
    }
}

//Actions
export const getEntities = () => async (dispatch) => {
    await axiosInstance.get(`${api.SUBJECT}?Page=1&PageSize=250&Sort=id_asc`)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.GET_SUBJECTS,
                payload: res.data,
            })
        })
        .catch(() => {
            window.location = '/error';
        })
}