import {Component} from "react";
import "../../../../style/pages/dashboard/workspace/manageusers/ManageUser.css";
import {Button, Col, FormControl, InputGroup, Pagination, Row, Table} from "react-bootstrap";
import {getEntities, searchUser, setUserState} from "./manageUserReducer";
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@material-ui/core";
import {ITEM_PER_PAGE} from "../../../../shared/constaints/pagination";

export class ManageUser extends Component {
    state = {
        open: false,
        username: '',
        dialogTitle: '',
        dialogContent: '',
        uid: '',
        order: true,
    }

    componentDidMount() {
        this.props.getEntities(this.props.currentPage);
    }

    handleClickOpen = (name, title, content, uid) => {
        this.setState({
            open: true,
            username: name,
            dialogTitle: title,
            dialogContent: content,
            uid: uid,
        })
    };

    handleClose = () => {
        this.setState({
            open: false
        })
    };

    handleConfirm = () => {
        this.props.setUserState(this.state.uid);
        this.setState({
            open: false
        })
    }

    handleClickPagination = (clickedPage) => {
        this.props.getEntities(clickedPage);
    }

    handleSearch = (event) => {
        this.props.searchUser(event.target.value);
    }

    mapper(user, index) {
        return (
            <tr key={user.id} style={{height: "20px"}}>
                <td className="text-center-v">
                    {(this.props.currentPage - 1) * ITEM_PER_PAGE + index + 1}
                </td>
                <td className="text-center-v">
                    {user.fullName}
                </td>
                <td className="text-center-v">
                    {user.dateOfBirth.substring(0, 10).split("-", 3).reverse().join("/")}
                </td>
                <td className="text-center-v">
                    {user.email}
                </td>
                <td className="text-center-v-h">
                    {user.isAdmin
                        ? (
                            <FontAwesomeIcon icon="check" color="green" size="2x"/>
                        ) : (
                            <FontAwesomeIcon icon="times" color="red" size="2x"/>
                        )}
                </td>
                <td className="text-center-v-h">
                    {
                        !user.isAdmin
                            ? (
                                user.isActive
                                    ? (
                                        <div>
                                            <Button variant="outline-danger"
                                                    className="btnUserState"
                                                    onClick={() =>
                                                        this.handleClickOpen(
                                                            user.fullName,
                                                            "Ban Confirmation",
                                                            "Do you want to ban",
                                                            user.id)}>
                                                Inactive
                                            </Button>
                                        </div>
                                    )
                                    : (
                                        <div>
                                            <Button variant="outline-success"
                                                    className="btnUserState"
                                                    onClick={() =>
                                                        this.handleClickOpen(user.fullName,
                                                            "Activate Confirmation",
                                                            "Do you want to active",
                                                            user.id)}>
                                                Active
                                            </Button>
                                        </div>
                                    )
                            )
                            : null
                    }
                </td>
            </tr>
        )
    }

    render() {
        const {entities, totalPages, totalElements} = this.props;

        let items = [];
        for (let number = 1; number <= totalPages; number++) {
            items.push(
                <Pagination.Item key={number}
                                 active={number === this.props.currentPage}
                                 onClick={() => this.handleClickPagination(number)}>
                    {number}
                </Pagination.Item>,
            );
        }
        return (
            <div>
                <Row className="manageUserTitle">
                    <Col xl={1}/>
                    <Col xl={5}>
                        <p>
                            Manage Users:
                        </p>
                    </Col>
                </Row>
                <Row>
                    <div>
                        <InputGroup className="mb-3">
                            <FormControl aria-describedby="basic-addon1"
                                         placeholder="Search name..."
                                         onChange={event => this.handleSearch(event)}
                                         className="searchBar"/>
                            <InputGroup.Append>
                                <Button className="searchAppend" disabled>
                                    <FontAwesomeIcon icon="search"/>
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </div>
                </Row>
                {totalElements === 0
                    ? <div className="notFound">
                        <p>
                            No element found
                        </p>
                    </div>
                    : <div>
                        <Row className="addTableMargin tableDisplay">
                            <Table responsive bordered striped className="tableHeight">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Full Name</th>
                                    <th>DoB</th>
                                    <th>Email</th>
                                    <th className="text-center">Is Admin</th>
                                    <th className="text-center">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.order
                                    ? entities.map((user, index) => this.mapper(user, index))
                                    : entities.map(user => user).reverse().map((user, index) => this.mapper(user, index))}
                                </tbody>
                            </Table>
                            <Dialog
                                open={this.state.open}
                                onClose={this.handleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                                BackdropProps={{style: {backgroundColor: 'rgba(0, 0, 0, 0.5)'}}}
                                PaperProps={{
                                    style: {
                                        backgroundColor: '#FFFFFF',
                                        boxShadow: 'none',
                                        width: '40%'
                                    },
                                }}
                            >
                                <DialogTitle
                                    id="alert-dialog-title">
                                    {this.state.dialogTitle}
                                </DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        {`${this.state.dialogContent} ${this.state.username}?`}
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.handleClose}
                                            variant="danger"
                                            style={{
                                                minWidth: "15%"
                                            }}>
                                        No
                                    </Button>
                                    <Button onClick={this.handleConfirm}
                                            autoFocus
                                            variant="primary"
                                            style={{
                                                minWidth: "15%"
                                            }}>
                                        Yes
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </Row>
                        <Row className="addTableMargin">
                            <Pagination>{items}</Pagination>
                        </Row>
                    </div>}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        currentPage: state.manageUser.currentPage,
        entities: state.manageUser.entities,
        totalPages: state.manageUser.totalPages,
        totalElements: state.manageUser.totalElements,
    }
}

const mapDispatchToProps = {
    getEntities,
    setUserState,
    searchUser
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageUser);