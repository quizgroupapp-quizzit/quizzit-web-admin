import {Component} from "react";
import {Button, Row} from "react-bootstrap";
import "../../../../style/pages/dashboard/workspace/summary/SummaryGroup.css"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {getGroupNumbers} from "./summaryReducer";
import {connect} from "react-redux";

export class SummaryGroups extends Component {
    componentDidMount() {
        this.props.getGroupNumbers();
    }

    render() {
        const {totalGroup, totalGroupActive} = this.props;
        return (
            <div>
                <Row>
                    <Button variant="outline-dark" className="btnGroupsShow">
                        <FontAwesomeIcon icon="users" size="3x"/> <br/>
                        Current Groups: &nbsp;
                        <span style={{
                            color: "#E9633B",
                            fontFamily: "Cambria serif"
                        }}>{totalGroup}</span>
                    </Button>
                    <Button variant="outline-dark" className="btnGroupsShow">
                        <FontAwesomeIcon icon="globe" size="3x"/> <br/>
                        Active Groups: &nbsp;
                        <span style={{
                            color: "#E9633B",
                            fontFamily: "Cambria serif"
                        }}>{totalGroupActive}</span>
                    </Button>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        totalGroup: state.summary.totalGroup,
        totalGroupActive: state.summary.totalGroupActive,
    }
}

const mapDispatchToProps = {
    getGroupNumbers,
}

export default connect(mapStateToProps, mapDispatchToProps)(SummaryGroups);