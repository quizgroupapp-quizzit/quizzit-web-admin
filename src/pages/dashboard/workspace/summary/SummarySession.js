import {Component} from "react";
import {Col, Image, Row} from "react-bootstrap";
import "../../../../style/pages/dashboard/workspace/summary/SummarySession.css"
import SummaryUsers from "./SummaryUsers";
import SummaryGroups from "./SummaryGroups";


class SummarySession extends Component {

    render() {
        return (
            <div>
                <Row className="generalInfo">
                    <Col xl={1}/>
                    <Col xl={5}>
                        <p>
                            Web Admin Activity:
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col xl={1}/>
                    <Col xl={4}>
                        <Image className="responsiveImage" src="summary.jpg" alt="summary" roundedCircle/>
                    </Col>
                    <Col xl={6} className="summarySection">
                        <Row>
                            <SummaryUsers/>
                        </Row>
                        <Row>
                            <SummaryGroups/>
                        </Row>
                    </Col>
                </Row>
            </div>

        )
    }
}


export default SummarySession;