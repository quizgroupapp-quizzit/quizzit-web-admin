import {Component} from "react";
import {Button, Row} from "react-bootstrap";
import "../../../../style/pages/dashboard/workspace/summary/SummaryUser.css"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {connect} from "react-redux";
import {getUserNumbers} from "./summaryReducer";

export class SummaryUsers extends Component {
    componentDidMount() {
        this.props.getUserNumbers();
    }

    render() {
        const {totalUsers, totalUserActive, totalAdmin} = this.props;
        return (
            <div>
                <Row>
                    <Button variant="outline-dark" className="btnUsersShow">
                        <FontAwesomeIcon icon="user-graduate" size="3x"/> <br/>
                        Current Users: &nbsp;
                        <span style={{
                            color: "#E9633B",
                            fontFamily: "Cambria serif"
                        }}>{totalUsers}</span>
                    </Button>
                    <Button variant="outline-dark" className="btnUsersShow">
                        <FontAwesomeIcon icon="headset" size="3x"/> <br/>
                        Active Users: &nbsp;
                        <span style={{
                            color: "#E9633B",
                            fontFamily: "Cambria serif"
                        }}>{totalUserActive}</span>
                    </Button>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        totalUsers: state.summary.totalUsers,
        totalUserActive: state.summary.totalUserActive,
        totalAdmin: state.summary.totalAdmin,
    }
}

const mapDispatchToProps = {
    getUserNumbers,
}

export default connect(mapStateToProps, mapDispatchToProps)(SummaryUsers);