import axiosInstance from "../../../../helpers/AxiosAPI";
import {API as api} from "../../../../shared/constaints/api";

const initialState = {
    totalUsers: 0,
    totalUserActive: 0,
    totalAdmin: 0,
    totalGroup: 0,
    totalGroupActive: 0,
}

export const ACTION_TYPES = {
    GET_USERS: 'GetUsersNumber',
    GET_GROUPS: 'GetGroupsNumber',
}

//Reducers
export const summaryReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.GET_GROUPS: {
            return {
                ...state,
                totalGroup: action.payload.totalGroup,
                totalGroupActive: action.payload.totalActive,
            }
        }
        case ACTION_TYPES.GET_USERS: {
            return {
                ...state,
                totalUsers: action.payload.totalUsers,
                totalUserActive: action.payload.totalActive,
                totalAdmin: action.payload.totalAdmin,
            }
        }
        default:
            return state;
    }
}

//Actions
export const getUserNumbers = () => async (dispatch) => {
    await axiosInstance.get(`${api.USER_TOTAL}`)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.GET_USERS,
                payload: res.data,
            })
        })
        .catch(() => {
            window.location = '/error';
        })
}

export const getGroupNumbers = () => async(dispatch) => {
    await axiosInstance.get(`${api.GROUP_TOTAL}`)
        .then(res => {
            dispatch({
                type: ACTION_TYPES.GET_GROUPS,
                payload: res.data,
            })
        })
        .catch(() => {
            window.location = '/error';
        })
}