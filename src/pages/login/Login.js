import {Component} from 'react';
import {Button, Form, Col, Row, Container, InputGroup} from "react-bootstrap";
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import "../../style/pages/login/Login.css"
import {signIn} from "../../shared/reducers/authReducer";
import {Redirect} from "react-router";

export class Login extends Component {
    state = {
        username: "",
        password: ""
    }



    handleLogin = (event) => {
        event.preventDefault();
        this.props.signIn(this.state)
    }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    render() {
        const {uid} = this.props;
        if (uid) return <Redirect to="/"/>
        return (
            <div>
                <Container fluid>
                    <Row>
                        <Col xl={4}/>
                        <Col xl={4}>
                            <p className="title">Admin Login</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col xl={4}/>
                        <Col xl={8}>
                            <Form onSubmit={event => this.handleLogin(event)}>
                                <Form.Group as={Row} controlId="formUsername">
                                    <Form.Label column sm={1} className="label">Username</Form.Label>
                                    <Col xl={5}>
                                        <InputGroup>
                                            <InputGroup.Prepend className="prepend">
                                                <InputGroup.Text className="prepend"><FontAwesomeIcon
                                                    icon="user"/></InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control type="text"
                                                          className="txtUsername"
                                                          name="username"
                                                          onChange={event => this.handleChange(event)}/>
                                        </InputGroup>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} controlId="formPassword">
                                    <Form.Label column sm={1} className="label">Password</Form.Label>
                                    <Col xl={5}>
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text><FontAwesomeIcon icon="lock"/></InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control type="password"
                                                          className="txtPassword"
                                                          name="password"
                                                          onChange={event => this.handleChange(event)}/>
                                        </InputGroup>
                                    </Col>
                                </Form.Group>
                                <Button variant="outline-secondary" className="loginButton" type="submit">
                                    Login
                                </Button>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const uid = state.firebase.auth.uid;
    return {
        uid: uid,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (cred) => dispatch(signIn(cred)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);