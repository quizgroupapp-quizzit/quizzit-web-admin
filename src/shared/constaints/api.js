export const API = {
    BASE_URL: "https://hieulnhcm.ddns.net:5001",
    USER: "/api/users",
    USER_TOTAL: "/api/users/total",
    ADMIN_AUTH: "/api/admin/login",
    GROUP: "/api/groups",
    GROUP_TOTAL: "/api/groups/total",
    MEMBER: "/api/members",
    SUBJECT: "/api/subjects",
    ADMIN_QUESTION: "/api/questions/admin",
    GENERAL_QUESTION: "/api/questions",
}