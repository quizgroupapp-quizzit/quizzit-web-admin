import {Container, Row, Col} from 'react-bootstrap';
import Image from '../../../image/error.png';
import "./ErrorScreen.css"
import {Component} from "react";

class ErrorScreen extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <img src={Image} alt="error" className="error-page"/>
                    </Col>
                    <Col className="text-container">
                        <div>
                            <span>Unexpected Error Occurred!</span>
                        </div>
                        <span>Please contact to </span>
                        <span className="red-text">Authorized Individuals</span>
                        <p/>
                        <div>
                            <span>
                                <a href="/">Go Back</a>
                            </span>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default ErrorScreen;