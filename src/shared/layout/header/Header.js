import {Component} from "react";
import {Image, Nav, Navbar} from 'react-bootstrap';
import './Header.css';
import {connect} from "react-redux";
import {signOut} from "../../reducers/authReducer";
import {Link, NavLink} from "react-router-dom";

class Header extends Component {
    handleClick = () => {
        this.props.signOut()
    }

    render() {
        return (
            <Navbar collapseOnSelect expand="md" variant="dark" sticky="top" className="headerLightColor">
                <Navbar.Brand href="/">
                    <Image src="logo.png" rounded width="40em" height="40em"/>
                </Navbar.Brand>
                <Nav className="mr-auto">
                    <NavLink to="/" className="nav-link headerFont">
                        Home
                    </NavLink>
                </Nav>
                <Nav className="ml-auto orientationOnMobile">
                    <Navbar.Text>
                        Hello Admin,
                    </Navbar.Text>
                    &nbsp;
                    <Link to="/login" className="nav-link" onClick={this.handleClick}>
                        Logout
                    </Link>
                </Nav>
            </Navbar>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut()),
    };
};

export default connect(null, mapDispatchToProps)(Header);