import {Col, Nav, Navbar, Row} from "react-bootstrap";
import {Component} from "react";
import "./Navigation.css"
import {NavLink} from "react-router-dom";

export class Navigation extends Component {

    render() {
        return (
            <Navbar collapseOnSelect expand="sm" variant="dark">
                <Navbar.Brand/>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="navigation">
                        <Row>
                            <Col xl={12}>
                                <NavLink exact to="/" className="nav-link addMarginNavItem">
                                    Dashboard
                                </NavLink>


                                <NavLink to="/manageUser" className="nav-link addMarginNavItem">
                                    Manage Users
                                </NavLink>

                                <NavLink to="/manageGroup" className="nav-link addMarginNavItem">
                                    Manage Groups
                                </NavLink>

                                <NavLink to="/manageQuestion" className="nav-link addMarginNavItem">
                                    Manage Question
                                </NavLink>

                                <NavLink to="/manageSubject" className="nav-link addMarginNavItem">
                                    Manage Subjects
                                </NavLink>

                                {/*<NavLink to="#" className="nav-link addMarginNavItem" disabled>*/}
                                {/*    Setting*/}
                                {/*</NavLink>*/}
                            </Col>
                        </Row>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default Navigation;