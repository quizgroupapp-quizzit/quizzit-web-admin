import {toast} from "react-toastify";
import axios from "axios";
import {API as api} from "../constaints/api";
import {token} from "../constaints/token";

export const ACTION_TYPES = {
    SIGN_IN: 'SIGN_IN',
    SIGN_IN_ERR: 'SIGN_IN_ERR',
    SIGN_OUT: 'SIGN_OUT'
}

//Reducers
export const authReducer = (state = {}, action) => {
    switch (action.type) {
        case ACTION_TYPES.SIGN_IN:
            toast.success("Logged in success!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return state;
        case ACTION_TYPES.SIGN_IN_ERR:
            toast.error("Wrong username or password!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return state;
        case ACTION_TYPES.SIGN_OUT:
            toast.info("Signed out!", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return state;
        default:
            return state;
    }
}

//Actions
export const signIn = cred => {
    return (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();
        axios.post(`${api.BASE_URL}${api.ADMIN_AUTH}`, {
            username: cred.username,
            password: cred.password
        })
            .then(response => firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION).then(() =>
            {
                // console.log(window.atob(response.data.customToken))
                firebase
                    .auth()
                    .signInWithCustomToken(response.data.customToken)
                    .then((userCredential) => {
                        const user = userCredential.user;
                        user.getIdToken()
                            .then(idToken => {
                                localStorage.setItem(token.CUSTOM_TOKEN, idToken);
                                dispatch({type: ACTION_TYPES.SIGN_IN, user});
                            })
                    })}))
            .catch(err => {
                dispatch({type: ACTION_TYPES.SIGN_IN_ERR}, err);
            });
    }
}

export const signOut = () => {
    return (dispatch, getState, {getFirebase}) => {
        const firebase = getFirebase();
        firebase
            .auth()
            .signOut()
            .then(() => {
                localStorage.removeItem(token.CUSTOM_TOKEN)
                dispatch({type: ACTION_TYPES.SIGN_OUT});
            });
    };
};