import {combineReducers} from "redux";
import {mainPageReducer} from '../../mainpage/MainPageReducer';
import {manageUserReducer} from "../../pages/dashboard/workspace/manageusers/manageUserReducer";
import {authReducer} from "./authReducer"
import {firebaseReducer} from "react-redux-firebase";
import {manageSubjectReducer} from "../../pages/dashboard/workspace/managesubjects/manageSubjectReducer";
import {manageGroupReducer} from "../../pages/dashboard/workspace/managegroups/manageGroupReducer";
import {manageQuestionReducer} from "../../pages/dashboard/workspace/managequestions/manageQuestionReducer";
import {summaryReducer} from "../../pages/dashboard/workspace/summary/summaryReducer";

const rootReducer = combineReducers({
    mainPage: mainPageReducer,
    manageUser: manageUserReducer,
    manageSubject: manageSubjectReducer,
    manageGroup: manageGroupReducer,
    auth: authReducer,
    firebase: firebaseReducer,
    manageQuestion: manageQuestionReducer,
    summary: summaryReducer,
})

export default rootReducer;